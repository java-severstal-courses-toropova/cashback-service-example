package org.example.sample;

import org.example.service.CashbackService;

public class Main {
    public static void main(String[] args) {
        CashbackService service = new CashbackService();
        int cashback = service.calculateCashback(105_25);
        System.out.println(cashback);
    }
}
